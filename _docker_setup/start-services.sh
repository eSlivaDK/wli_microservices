#!/usr/bin/env bash

eval `docker-machine env manager1`

array=(
    './wli-gateway'
  './competitionService'
  './userService'
)

# we go to the root of the project
cd ..

for ((i = 0; i < ${#array[@]}; ++i)); do
  # we go into a specific folder
  cd ${array[$i]}

  # we start our docker services
  sh ./start-service.sh

  # and we go back to the root again :D
  cd ..
done
#!/usr/bin/env bash
docker rm -f competition-service
docker rmi competition-service
docker image prune
docker volume prune
# the previous commands are for removing existing containers, 
# and image, then we clean our environment and finally 
# we creat or re-build our image
docker build -t competition-service .
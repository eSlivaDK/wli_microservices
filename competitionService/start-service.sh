#!/usr/bin/env bash
docker service create --replicas 3 --name competition-service -l=apiRoute='/competitions' -p 3001:3000 --env-file env edwinsilvadk/competition-service
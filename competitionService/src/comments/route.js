const router = require("express").Router();
const controller = require("./controller");

router.get("/getAll/:competitionId/:ideaId", controller.getPaginatedComments);
router.post("/create", controller.createComment);
router.delete("/delete/:authId", controller.deleteComment);

module.exports = router;

const admin = require("firebase-admin");
const db = admin.firestore();

async function getAllCompetitions(_req, res) {
  let userId = await req.params.userId;
  try {
    const competitionRef = await db
      .collection("competitions")
      .where("participants", "array-contains", userId)
      .orderBy("createdAt", "desc")
      .get();
    const competitions = [];
    competitionRef.forEach(doc => {
      competitions.push({
        data: doc.data()
      });
    });

    return res.json({
      success: true,
      data: competitions
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

async function getCompetition(req, res) {
  let competitionId = await req.params.competitionId;
  console.log("TCL: getCompetition -> competitionId", competitionId);
  try {
    const competitionRef = db.collection("competitions").doc(competitionId);

    const competition = (await competitionRef.get()).data();

    return res.json({
      success: true,
      data: competition
    });
  } catch (error) {
    console.log("TCL: getCompetition ->  error", error);
    return res.json({
      success: false,
      data: error
    });
  }
}
async function createCompetition(req, res) {
  let competition = await req.body;
  const serverTimeStamp = admin.firestore.Timestamp.now();
  try {
    const competitionRef = await db.collection("competitions").doc();
    const competitionId = await competitionRef.id;
    competition.id = competitionId;
    competition.createdAt = serverTimeStamp;
    await competitionRef.add(competition);
    return res.json({
      success: true,
      data: competition
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function addUserToCompetition(req, res) {
  let competitionId = await req.body.competitionId;
  let userId = await req.body.userId;
  try {
    const competitionRef = db.collection("competitions").doc(competitionId);

    await competitionRef.update({
      participants: admin.firestore.FieldValue.arrayUnion(userId),
      unReadCompetition: admin.firestore.FieldValue.arrayUnion(userId)
    });
    return res.json({
      success: true,
      data: "Added user: " + userId + " to competition: " + competitionId
    });
  } catch (error) {
    console.log("TCL: addUserToCompetition -> error", error);
    return res.json({
      success: false,
      data: error
    });
  }
}
async function removeUserFromCompetition(req, res) {
  let competitionId = await req.params.competitionId;
  let userId = await req.params.userId;
  try {
    const competitionRef = db.collection("competitions").doc(competitionId);
    await competitionRef.update({
      participants: admin.firestore.FieldValue.arrayRemove(userId),
      unReadCompetition: admin.firestore.FieldValue.arrayRemove(userId)
    });
    return res.json({
      success: true,
      data: "Removed user : " + userId + " From competition: " + competitionId
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function getCompetitionViaPinCode(req, res) {
  let competitionPin = await req.params.pin;

  try {
    const competitionRef = db
      .collection("competitions")
      .where("PIN", "==", competitionPin)
      .limit(1);
    const querySnapshot = await competitionRef.get();
    const competition = querySnapshot.docs[0].data();
    return res.json({
      success: true,
      data: competition
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

module.exports = {
  getCompetition,
  getAllCompetitions,
  createCompetition,
  addUserToCompetition,
  removeUserFromCompetition,
  getCompetitionViaPinCode
};

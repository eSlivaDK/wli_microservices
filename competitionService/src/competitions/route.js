const router = require("express").Router();
const controller = require("./controller");

router.get("/via_id/:competitionId", controller.getCompetition);
router.get("/via_pin/:pin", controller.getCompetitionViaPinCode);
router.get("/all/:userId", controller.getAllCompetitions);
router.post("/create", controller.createCompetition);
router.put("/add_user", controller.addUserToCompetition);
router.delete(
  "/remove_user/:competitionId/:userId",
  controller.removeUserFromCompetition
);
module.exports = router;

//The order of the import matter
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const serviceAccount = require("../dev-competition-service-account-file.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://dev-competition-service.firebaseio.com"
});
const server = require("./server");

const api = functions
  .runWith({
    memory: "2GB",
    timeoutSeconds: 120
  })
  .https.onRequest(server);

module.exports = {
  api
};

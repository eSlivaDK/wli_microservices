const router = require("express").Router();
const controller = require("./controller");

router.get("/getAll/:competitionId", controller.getAllIdeas);
router.get("/getSingle/:competitionId/:ideaId", controller.getIdea);
router.post("/create", controller.createIdea);
router.put("/loveIdea", controller.loveIdea);
router.delete("/delete/:competitionId/:ideaId", controller.deleteIdea);

module.exports = router;

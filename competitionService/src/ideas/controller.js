const admin = require("firebase-admin");
const db = admin.firestore();

async function getAllIdeas(_req, res) {
  let competitionId = await _req.params.competitionId;
  let ideas = [];
  try {
    let ideaRef;
    const competitionRef = db.collection("competitions").doc(competitionId);
    const competition = (await competitionRef.get()).data();
    if (!competition.ended) {
      ideaRef = await competitionRef.collection("ideas", ref => {
        return ref
          .orderBy("loveCount", "desc")
          .orderBy("createdAt", "asc")
          .get();
      });
    } else {
      ideaRef = await competitionRef.collection("ideas").get();
    }
    ideaCollection = ideaRef;
    await ideaCollection.forEach(doc => {
      ideas.push(doc.data());
    });

    return res.json({
      success: true,
      data: {
        ideas,
        competition
      }
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function getIdea(_req, res) {
  let competitionId = await _req.params.competitionId;
  let ideaId = await _req.params.ideaId;
  try {
    let ideaRef;
    const competitionRef = db.collection("competitions").doc(competitionId);
    const competition = (await competitionRef.get()).data();

    ideaRef = await competitionRef
      .collection("ideas")
      .doc(ideaId)
      .get();

    const idea = ideaRef.data();

    return res.json({
      success: true,
      data: {
        idea,
        competition
      }
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function loveIdea(_req, res) {
  const idea = await _req.body;
  const ideaId = idea.id;
  const competitionId = idea.competitionId;
  const userId = idea.authorId;
  try {
    let ideaRef;
    const competitionRef = db.collection("competitions").doc(competitionId);
    ideaRef = await competitionRef.collection("ideas").doc(ideaId);
    await ideaRef.update({
      lovedBy: firestore.FieldValue.arrayUnion(userId),
      watchers: firestore.FieldValue.arrayUnion(userId)
    });

    return res.json({
      success: true,
      data: {
        idea,
        competition
      }
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

async function deleteIdea(_req, res) {
  let competitionId = await _req.params.competitionId;
  let ideaId = await _req.params.ideaId;
  try {
    const competitionRef = db.collection("competitions").doc(competitionId);

    ideaRef = await competitionRef
      .collection("ideas")
      .doc(ideaId)
      .delete();
    return res.json({
      success: true,
      data: "Deleted idea : " + ideaId + " From competition: " + competitionId
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

async function createIdea(req, res) {
  const idea = await req.body;
  const serverTimeStamp = admin.firestore.Timestamp.now();
  const competitionId = idea.competitionId;
  try {
    const competitionRef = await db
      .collection("competitions")
      .doc(competitionId);
    const ideaRef = await competitionRef.collection("ideas").doc();
    idea.ideaRef.id;
    idea.createdAt = serverTimeStamp;
    await ideaRef.add(idea);
    return res.json({
      success: true,
      data: idea
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function updateIdea(_req, res) {
  const idea = await _req.body;
  const ideaId = idea.id;
  const competitionId = idea.competitionId;
  try {
    let ideaRef;
    const competitionRef = db.collection("competitions").doc(competitionId);
    ideaRef = await competitionRef.collection("ideas").doc(ideaId);
    await ideaRef.update(idea);
    return res.json({
      success: true,
      data: {
        idea
      }
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

module.exports = {
  loveIdea,
  createIdea,
  getIdea,
  getAllIdeas,
  deleteIdea,
  updateIdea
};

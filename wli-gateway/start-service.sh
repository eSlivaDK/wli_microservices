#!/usr/bin/env bash
docker service create --replicas 3 --name wli-gateway-service -l=apiRoute='/' -p 3000:3000 --env-file env edwinsilvadk/wli-gateway-service
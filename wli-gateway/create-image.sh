#!/usr/bin/env bash
docker rm -f wli-gateway-service
docker rmi wli-gateway-service
docker image prune
docker volume prune
# the previous commands are for removing existing containers, 
# and image, then we clean our environment and finally 
# we creat or re-build our image
docker build -t wli-gateway-service .
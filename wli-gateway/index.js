//const functions = require("firebase-functions");
const path = require("path");
const gateway = require("express-gateway");
console.log("TCL: __dirname", __dirname);

gateway()
  .load(path.join(__dirname, "config"))
  .run();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

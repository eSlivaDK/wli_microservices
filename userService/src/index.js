const functions = require("firebase-functions");
const admin = require("firebase-admin");
require("dotenv").config();
const serviceAccount = require("../dev-user-service-account-file.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.DATABASE_URL,
  storageBucket: process.env.STORAGE_BUCKET
});
const server = require("./server");

const api = functions
  .runWith({
    memory: "2GB",
    timeoutSeconds: 120
  })
  .https.onRequest(server);
module.exports = {
  api
};

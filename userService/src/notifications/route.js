const router = require("express").Router();
const controller = require("./controller");

router.get("/get/:userId", controller.getPaginatedNotifications);
router.put("/changeStatus", controller.updateNotificationStatus);
router.post("/add/:userId", controller.addNotification);
router.delete("/delete/:userId/:notificationId", controller.deleteNotification);

module.exports = router;

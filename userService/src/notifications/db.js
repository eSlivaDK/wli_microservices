const admin = require("firebase-admin");
const db = admin.firestore();
const userCollectionName = "users";
const notificationCollectionName = "notifications";

async function getPaginatedNotifications(userId, index) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef
    .collection(notificationCollectionName)
    .orderBy("createdAt")
    .limit(index);

  let first = notificationRef;
  try {
    let paginate = await first.get();

    // Get the last document
    let last = paginate.docs[paginate.docs.length - 1];
    startIndex = last.get("createdAt");
    // Construct a new query starting at this document.
    // Note: this will not have the desired effect if multiple
    // cities have the exact same population value.
    let notifications = await userRef
      .collection("notifications")
      .orderBy("createdAt")
      .startAt(startIndex)
      .limit(10)
      .get();

    return notifications;
  } catch (error) {
    return error;
  }
}

async function addNotification(userId, notification) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef.collection(notificationCollectionName);
  try {
    await notificationRef.add(notification);
    await notificationRef.update({
      createdAt: admin.firestore.FieldValue.serverTimestamp()
    });
    data = "user: " + userId + " received a new notification";
    return data;
  } catch (error) {
    return error;
  }
}
async function updateNotificationStatus(userId, notificationId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef
    .collection(notificationCollectionName)
    .doc(notificationId);
  try {
    await notificationRef.update({
      read: true
    });
    data = "notification: " + notificationId + " has been read";

    return data;
  } catch (error) {
    return error;
  }
}
async function deleteNotification(userId, notificationId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef
    .collection(notificationCollectionName)
    .doc(notificationId);
  try {
    await notificationRef.delete();
    data = "notification: " + notificationId + " has been deleted";
    return data;
  } catch (error) {
    return error;
  }
}
module.exports = {
  getPaginatedNotifications,
  addNotification,
  updateNotificationStatus,
  deleteNotification
};

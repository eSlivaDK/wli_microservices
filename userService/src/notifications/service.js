const db = require("./db");

async function getPaginatedNotifications(userId, index) {
  let notificationsArray = [];
  const notifications = await db.getPaginatedNotifications(userId, index);
  notifications.forEach(notification => {
    notificationsArray = notification.data();
  });
  return notificationsArray;
}
async function addNotification(userId, notification, type) {
  switch (type) {
    case "badge":
      break;
    case 2:
      break;
    case 3:
      break;
    case 4:
      break;
    case 5:
      break;

    default:
      break;
  }
  const res = await db.addNotification(userId, notification);
  return res;
}
async function updateNotificationStatus(userId, notificationId) {
  const res = await db.updateNotificationStatus(userId, notificationId);
  return res;
}
async function deleteNotification(userId, notificationId) {
  const res = await db.deleteNotification();
  return res;
}

module.exports = {
  getPaginatedNotifications,
  addNotification,
  updateNotificationStatus,
  deleteNotification
};

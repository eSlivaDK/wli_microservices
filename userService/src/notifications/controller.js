const notification_service = require("./service");

async function getPaginatedNotifications(_req, res) {
  let userId = _req.params.userId;
  const index = parseInt(_req.query.limit) || 1;

  try {
    return res.json({
      success: true,
      data: await notification_service.getPaginatedNotifications(userId, index)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}

async function addNotification(req, res) {
  const userId = req.params.userId;
  const notification = req.body;
  try {
    return res.json({
      success: true,
      data: await notification_service.addNotification(userId, notification)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}
async function updateNotificationStatus(req, res) {
  const body = req.body;
  const userId = body.userId;
  const notificationId = body.notificationId;
  try {
    return res.json({
      success: true,
      data: await notification_service.updateNotificationStatus(
        userId,
        notificationId
      )
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}
async function deleteNotification(req, res) {
  const userId = req.params.userId;
  const notificationId = req.params.notificationId;
  try {
    return res.json({
      success: true,
      data: await notification_service.deleteNotification(
        userId,
        notificationId
      )
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}
module.exports = {
  getPaginatedNotifications,
  addNotification,
  updateNotificationStatus,
  deleteNotification
};

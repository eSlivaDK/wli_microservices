require("dotenv").config();
const amqp = require("amqplib");
// RabbitMQ connection string
const messageQueueConnectionString = process.env.CLOUDAMQP_URL;
console.log("TCL: messageQueueConnectionString", messageQueueConnectionString);
async function setup() {
  console.log("Setting up RabbitMQ Exchanges/Queues");
  // connect to RabbitMQ Instance
  try {
    let connection = await amqp.connect(messageQueueConnectionString);

    // create a channel
    let channel = await connection.createChannel();

    // create exchange
    await channel.assertExchange("user.tx", "topic", { durable: true });

    // create user queues

    await channel.assertQueue("user-created-queue", { durable: true });
    await channel.assertQueue("user-updated-queue", { durable: true });
    await channel.assertQueue("user-deleted-queue", { durable: true });
    await channel.assertQueue("user-requested.badge-queue", {
      durable: true
    });

    // bind queues
    await channel.bindQueue("user-created-queue", "user.tx", "created");
    await channel.bindQueue("user-updated-queue", "user.tx", "updated");
    await channel.bindQueue("user-deleted-queue", "user.tx", "deleted");
    console.log("MQTT UserService Setup DONE");
  } catch (error) {
    console.log("TCL: setup -> error", error);
  }
}

module.exports = {
  setup
};

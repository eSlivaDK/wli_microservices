require("dotenv").config();
const amqp = require("amqplib");
// RabbitMQ connection string
const messageQueueConnectionString = process.env.CLOUDAMQP_URL;
let connection;

// create a channel
let channel;
async function set_consumers() {
  console.log("Setting up RabbitMQ Exchanges/Queues");
  // connect to RabbitMQ Instance
  try {
    connection = await amqp.connect(messageQueueConnectionString);
    channel = await connection.createChannel();
  } catch (error) {
    console.log("TCL: setup -> error", error);
  }
}

module.exports = { set_consumers };

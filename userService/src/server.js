const express = require("express");
const cors = require("cors");
const app = express();
const allowedOrigins = ["http://localhost:4200", "http://localhost:8080"];
const PORT = process.env.PORT || 3000;
const HOST = "0.0.0.0";
const MQTT_setup = require("./MQService/setupMqtt");
const MQTT_consumer = require("./MQService/consumers");
app.use(cors({ origin: true }));
app
  .use(express.urlencoded({ extended: true }))
  .use(express.json())
  .use("/users", require("./users/route"))
  .use("/notifications", require("./notifications/route"))
  .get("*", (_, res) =>
    res.status(404).json({ success: false, data: "Endpoint not found" })
  );
MQTT_setup.setup();
MQTT_consumer.set_consumers();
app.listen(PORT, HOST);
module.exports = app;

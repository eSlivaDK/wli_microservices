const user_service = require("./service");
const notification_service = require("../notifications/service");
require("dotenv").config();

async function updateUserDoc(req, res) {
  const body = req.body;
  try {
    return res.json({
      success: true,
      data: await user_service.updateUserDoc(body)
    });
  } catch (d) {
    return res.json({
      success: false,
      data: error
    });
  }
}

async function getUserDoc(req, res) {
  const userId = req.params.userId;
  try {
    return res.json({
      success: true,
      data: await user_service.getUserDoc(userId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

async function createUserDoc(req, res) {
  const body = req.body;
  const userId = body.uid;
  try {
    const user = await user_service.createUserDoc(body);
    const notification = await user_service.addBadgeToUser(userId, 1);
    await notification_service.addNotification(userId, notification, "badge");
    //publishToChannel will be used by the marketing service
    await user_service.publishToChannel("created", "user.tx", user);

    return res.json({
      success: true,
      data: user
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}

async function addCompetitionToUser(req, res) {
  const body = req.body;
  const competitionId = body.competitionId;
  const userId = body.userId;

  try {
    return res.json({
      success: true,
      data: await user_service.addCompetitionToUser(userId, competitionId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}
async function addBadgeToUser(req, res) {
  const body = req.body;
  const badgeId = body.badgeId;
  const userId = body.userId;
  try {
    return res.json({
      success: true,
      data: await user_service.addBadgeToUser(userId, badgeId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function addIdeaToUser(req, res) {
  const body = req.body;
  const userId = body.userId;
  const ideaId = body.ideaId;
  try {
    return res.json({
      success: true,
      data: await user_service.addIdeaToUser(userId, ideaId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

module.exports = {
  addCompetitionToUser,
  addBadgeToUser,
  addIdeaToUser,
  createUserDoc,
  getUserDoc,
  updateUserDoc
};

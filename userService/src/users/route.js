const router = require("express").Router();
const controller = require("./controller");
router.post("/create", controller.createUserDoc);
router.get("/get/:userId", controller.getUserDoc);
router.put("/newBadge", controller.addBadgeToUser);
router.put("/newCompetition", controller.addCompetitionToUser);
router.put("/newIdea", controller.addIdeaToUser);
router.put("/update", controller.updateUserDoc);

module.exports = router;

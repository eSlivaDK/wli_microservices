const admin = require("firebase-admin");
const db = admin.firestore();
const userCollectionName = "users";

async function updateUserDoc(user) {
  const userId = user.uid;
  let userRef = db.collection(userCollectionName).doc(userId);
  try {
    await userRef.update(user);
    return user;
  } catch (error) {
    return error;
  }
}

async function getUserDoc(userId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  try {
    const doc = await userRef.get();
    if (doc.exists) {
      const data = doc.data();
      return data;
    } else {
      return undefined;
    }
  } catch (error) {
    return error;
  }
}

async function createUserDoc(user) {
  const userId = user.uid;
  const serverTimeStamp = db.firestore.Timestamp.now();
  user.createdAt = serverTimeStamp;
  const userRef = db.collection(userCollectionName).doc(userId);
  try {
    await userRef.set(user);
    return user;
  } catch (error) {
    return error;
  }
}

async function addCompetitionToUser(userId, competitionId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  try {
    await userRef.update({
      myCompetitions: admin.firestore.FieldValue.arrayUnion(competitionId)
    });
    const data = competitionId + " was added to user: " + userId;
    return data;
  } catch (error) {
    return error;
  }
}
async function addBadgeToUser(userId, badgeId) {
  const userRef = db.collection(userCollectionName).doc(userId);

  try {
    await userRef.update({
      myBadges: admin.firestore.FieldValue.arrayUnion(badgeId)
    });
    const data = "New badge";

    return data;
  } catch (error) {
    return error;
  }
}
async function addIdeaToUser(userId, ideaId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  try {
    await userRef.update({
      myIdeas: admin.firestore.FieldValue.arrayUnion(ideaId)
    });
    const data = ideaId + " was added to user: " + userId;

    return data;
  } catch (error) {
    return error;
  }
}
async function checkIfUserPinExist(pin) {
  const userRef = db.collection(userCollectionName).where("PIN", "==", pin);
  return await userRef.get();
}
module.exports = {
  addCompetitionToUser,
  addBadgeToUser,
  addIdeaToUser,
  createUserDoc,
  getUserDoc,
  updateUserDoc,
  checkIfUserPinExist
};

require("dotenv").config();
const db = require("./db");
const amqp = require("amqplib");
const messageQueueConnectionString = process.env.CLOUDAMQP_URL;

async function addCompetitionToUser(userId, competitionId) {
  return await db.addCompetitionToUser(userId, competitionId);
}

async function addBadgeToUser(userId, badgeId) {
  let id;
  let title;
  let description;
  switch (badgeId) {
    case 1:
      id = 1;
      title = "Welcome on board";
      description = "Given for joining We Love Ideas";
      read = false;
      break;
    case 2:
      id = 2;
      title = "Ideator";
      description = "Given for creating your first idea";
      read = false;
      break;

    default:
      break;
  }
  const badge = {
    id,
    title,
    description,
    read
  };
  await db.addBadgeToUser(userId, badgeId);
  return badge;
}
async function addIdeaToUser(userId, ideaId) {
  return await db.addIdeaToUser(userId, ideaId);
}
async function createUserDoc(user) {
  const userPincode = await generateUserPincode();
  let userObject = {
    PIN: userPincode,
    competitionParticipation: 0,
    createdAt: serverTimeStamp,
    email: user.email,
    firstName: user.firstName,
    lastName: user.lastName,
    myBadges: [],
    myCompetitions: [],
    profilePicture: "profiles/" + userPincode,
    provider: user.provider,
    uid: user.uid
  };

  return await db.createUserDoc(userObject);
}
async function getUserDoc(userId) {
  return await db.getUserDoc(userId);
}
async function updateUserDoc(user) {
  return await db.updateUserDoc(user);
}
async function generateUserPincode() {
  let result = "w";
  const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < 5; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  const querysnapshot = await checkIfUserPinExist(result);

  const pinExist = querysnapshot.empty;
  if (pinExist) {
    return result;
  } else {
    generateUserPincode();
  }
}
async function checkIfUserPinExist(userPin) {
  return await db.checkIfUserPinExist(userPin);
}
async function publishToChannel(routingKey, exchangeName, data) {
  // connect to Rabbit MQ and create a channel
  const connection = await amqp.connect(messageQueueConnectionString);
  const channel = await connection.createConfirmChannel();
  try {
    channel.publish(
      exchangeName,
      routingKey,
      Buffer.from(JSON.stringify(data), "utf-8")
    );
    // This line doesn't run until the server responds to the publish
    await channel.close();
    // This line doesn't run until the client has disconnected without error
  } catch (e) {
    // Do something about it!
    console.log(e.stack);
  }
}

module.exports = {
  addCompetitionToUser,
  addBadgeToUser,
  addIdeaToUser,
  createUserDoc,
  getUserDoc,
  updateUserDoc,
  publishToChannel
};

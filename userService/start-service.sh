#!/usr/bin/env bash
docker service create --replicas 3 --name user-service -l=apiRoute='/users' -p 3002:3000 --env-file env edwinsilvadk/user-service
#!/usr/bin/env bash
docker rm -f user-service
docker rmi user-service
docker image prune
docker volume prune
# the previous commands are for removing existing containers, 
# and image, then we clean our environment and finally 
# we creat or re-build our image
docker build -t user-service .